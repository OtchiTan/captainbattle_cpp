﻿#pragma once

#include "CoreMinimal.h"
#include "Construction/Structs/CBBuildData.h"
#include "Level/Structs/CBFloorData.h"
#include "Level/Structs/CBHighlightedTile.h"
#include "Player/Structs/CBResourceData.h"
#include "UObject/Object.h"
#include "CBGameInstance.generated.h"

UCLASS()
class CAPTAINBATTLE_API UCBGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UCBGameInstance();
	
	UPROPERTY(BlueprintReadWrite)
	UDataTable* FloorTable = nullptr;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<FCBFloorData> FloorsData;
	
	UPROPERTY(BlueprintReadWrite)
	UDataTable* ResourceTable = nullptr;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<FCBResourceData> ResourceData;
	
	UPROPERTY(BlueprintReadWrite)
	UDataTable* BuildTable = nullptr;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<FCBBuildData> BuildData;
	
	UPROPERTY(BlueprintReadWrite)
	UDataTable* HighlightedTileTable = nullptr;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<FCBHighlightedTile> HighlightedTileData;

	UFUNCTION(BlueprintCallable,BlueprintPure)
	FCBResourceData GetResourceData(const ECBResourceType& Type) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	FCBBuildData GetBuildData(const ECBBuildType& Type) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	FCBHighlightedTile GetHighlightedTile(const ECBHighlightType& Type) const;
};
