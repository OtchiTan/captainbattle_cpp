#pragma once

#include "CoreMinimal.h"
#include "Core/Level/Structs/CBTile.h"
#include "Components/ActorComponent.h"
#include "Core/Level/Enums/CBTravelable.h"
#include "CBGridMovement.generated.h"

class ACBWorldManager;
class ACBPawn;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CAPTAINBATTLE_API UCBGridMovement : public UActorComponent
{
	GENERATED_BODY()

public:
	UCBGridMovement();

protected:
	virtual void BeginPlay() override;

	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY()
	ACBWorldManager* WorldManager = nullptr;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 MovementPointMax = 3;

	UPROPERTY(BlueprintReadWrite)
	int32 MovementPoint = 3;

	UPROPERTY(EditAnywhere)
	float MovementSpeed = 3;

	double MovementProgress = 0.0f;

	UPROPERTY(ReplicatedUsing=OnRep_PathfindingData)
	TArray<FIntPoint> PathfindingData = {};

	UFUNCTION()
	void OnRep_PathfindingData();

	UFUNCTION(Client,Reliable, BlueprintCallable)
	void Client_FindPath(FIntPoint Target, bool ShowReachable);
	
	UFUNCTION(Server,Reliable)
	void Server_FindPath(FIntPoint Target, bool ShowReachable);

	UFUNCTION(Client,Reliable)
	void Client_ShowPath();

	UPROPERTY(BlueprintReadWrite)
	bool bShowReachable = false;
	UPROPERTY(BlueprintReadWrite)
	bool bShowPath = true;
	
	FIntPoint OriginTile = {};
	FIntPoint TargetedTile = {};

	UPROPERTY(BlueprintReadWrite,Replicated)
	ACBPawn* Pawn = nullptr;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ECBTravelable> Travelable = T_None;

	FVector TargetLocation = {};

	bool bIsMoving = false;
	
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_MoveToTile();

	UFUNCTION(Server,Reliable)
	void Server_NextMove();

	UFUNCTION()
	void OnReachTile();

	UFUNCTION(Server,Reliable)
	void Server_FinishMove();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<FCBTile> GetNeighbours() const;
};
