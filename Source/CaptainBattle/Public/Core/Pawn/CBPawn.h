#pragma once

#include "CoreMinimal.h"
#include "Core/Level/Structs/CBTileResource.h"
#include "Core/Player/Enums/CBResourceType.h"
#include "Core/Player/Structs/CBResourceData.h"
#include "CBPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMovementSuccess);

class UCBGridMovement;

UCLASS()
class CAPTAINBATTLE_API ACBPawn : public AActor
{
	GENERATED_BODY()

public:
	ACBPawn();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	FString Name = "Pawn";
	
	virtual void Tick(float DeltaTime) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Server,Reliable)
	void Server_Init();

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	UCBGridMovement* GridMovement;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(BlueprintReadWrite,Replicated)
	FIntPoint TileIndex;

	UFUNCTION(Client, Reliable)
	void Client_MovementSuccess();

	UPROPERTY(BlueprintAssignable)
	FOnMovementSuccess OnMovementSuccess;

	UPROPERTY(BlueprintReadOnly)
	TMap<TEnumAsByte<ECBResourceType>, int32> Resources;

	void AddResources(const FCBTileResource& TileResource);
	void RemoveResources(const FCBTileResource& TileResource);
};
