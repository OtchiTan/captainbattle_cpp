﻿#pragma once

#include "CoreMinimal.h"
#include "CBBuildType.generated.h"

UENUM(BlueprintType)
enum ECBBuildType
{	
	BT_None			UMETA(DisplayName = "None"),
	BT_Dock			UMETA(DisplayName = "Dock")
};