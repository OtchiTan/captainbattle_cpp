#pragma once

#include "Core/Construction/Enums/CBBuildType.h"
#include "Core/Level/Enums/CBTileType.h"
#include "Core/Level/Enums/CBTravelable.h"
#include "Core/Player/Enums/CBResourceType.h"
#include "Engine/DataTable.h"
#include "CBBuildData.generated.h"

class ACBConstruction;

USTRUCT(BlueprintType)
struct CAPTAINBATTLE_API FCBBuildData : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBBuildType> Type;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName Name;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<TEnumAsByte<ECBTileType>> ConstructableTiles;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TMap<TEnumAsByte<ECBResourceType>, int32> QuantitiesToBuild;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UTexture2D* IconTexture;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<ACBConstruction> Construction;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBTravelable> Travelable;
};
