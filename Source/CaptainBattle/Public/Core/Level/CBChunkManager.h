#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Enums/CBHighlightType.h"
#include "CBChunkManager.generated.h"

class ACBWorldManager;
class ACBChunk;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CAPTAINBATTLE_API UCBChunkManager : public UActorComponent
{
	GENERATED_BODY()

public:
	UCBChunkManager();

protected:
	virtual void BeginPlay() override;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(Client,Reliable)
	void Client_SpawnChunks();

	UFUNCTION(Server,Reliable)
	void Server_SpawnChunks();

	UFUNCTION(Client,Reliable)
	void Client_RecomputeChunks(FIntPoint NewOccupiedChunk);

	UFUNCTION(Server,Reliable)
	void Server_MoveChunk(ACBChunk* Chunk, FIntPoint NewLocation);

	UPROPERTY()
	TMap<FIntPoint,ACBChunk*> LoadedChunks = {};
	FIntPoint OccupiedChunk = {};
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<ACBChunk> ChunkClass = nullptr;

	UPROPERTY(BlueprintReadWrite)
	ACBWorldManager* WorldManager = nullptr;

	UFUNCTION(Client,Reliable)
	void Client_SetWorldManager();
	
	const int32 ChunkSize = 20;

	// Highlight des tiles

	UFUNCTION(BlueprintCallable, Client, Unreliable)
	void Client_HighlightTile(FIntPoint WorldIndex, ECBHighlightType HighlightType);

	UFUNCTION(BlueprintCallable, Client, Unreliable)
	void Client_SelectTile(FIntPoint WorldIndex);

	UFUNCTION(BlueprintCallable, Client, Unreliable)
	void Client_ClearHighlightedTiles(ECBHighlightType HighlightType);
	
	TMap<ECBHighlightType,TSet<FIntPoint>> HighlightedTiles = {};
};
