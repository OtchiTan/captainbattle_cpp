#pragma once

#include "CoreMinimal.h"
#include "Structs/CBPathfindingData.h"
#include "Components/ActorComponent.h"
#include "Enums/CBTravelable.h"
#include "CBPathfinding.generated.h"

struct FCBFloorData;
class ACBWorldManager;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CAPTAINBATTLE_API UCBPathfinding : public UActorComponent
{
	GENERATED_BODY()

public:
	UCBPathfinding();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite)
	ACBWorldManager* WorldManager = nullptr;

	UFUNCTION(BlueprintCallable)
	TArray<FIntPoint> FindPath(FIntPoint Start, FIntPoint Target, TEnumAsByte<ECBTravelable> Travelable, bool bReturnReachable = false, int32 PathLength = -1);

	bool IsInputDataValid() const;

	void DiscoverTile(const FCBPathfindingData& TilePathData);

	static int32 GetMinimumCostBetweenTwoTiles(FIntPoint Index1, FIntPoint Index2);

	TArray<FIntPoint> GeneratePath() const;

	FCBPathfindingData PullCheapestTileOutOfDiscoveredList();

	bool AnalyseNextDiscoveredTile();

	UFUNCTION(BlueprintCallable,BlueprintPure)
	TArray<FCBPathfindingData> GetNeighborIndexes(FIntPoint Index, TEnumAsByte<ECBTravelable> Travelable);

	bool DiscoverNextNeighbor();

	void InsertTileInDiscoveredArray(const FCBPathfindingData& TileData);

	void ClearGeneratedData();

	int32 GetTileSortingCost(const FCBPathfindingData& TileData);

	bool IsTravelable(FIntPoint Index) const;
	bool IsTravelable(FIntPoint Index, FCBFloorData& FloorData) const;

	TEnumAsByte<ECBTravelable> Traveler = T_None;

	UPROPERTY(BlueprintReadWrite)
	int32 MaxPathLength = 0;

	bool bReturnReachableTiles = false;

	UPROPERTY()
	FIntPoint StartIndex = {};
	
	UPROPERTY()
	FIntPoint TargetIndex = {};
	
	UPROPERTY(BlueprintReadWrite)
	TMap<FIntPoint,FCBPathfindingData> PathfindingData = {};
	
	UPROPERTY()
	TArray<FIntPoint> DiscoveredTileIndexes = {};
	
	UPROPERTY()
	TArray<int32> DiscoveredTileSortingCost = {};
	
	UPROPERTY()
	TArray<FIntPoint> AnalysedTileIndexes = {};

	UPROPERTY()
	FCBPathfindingData CurrentDiscoveredTile = {};

	UPROPERTY()
	TArray<FCBPathfindingData> CurrentNeighbors = {};
	
	UPROPERTY()
	FCBPathfindingData CurrentNeighbor = {};
	
};
