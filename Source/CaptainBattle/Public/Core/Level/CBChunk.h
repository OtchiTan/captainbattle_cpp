// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/Player/CBPlayerController.h"
#include "GameFramework/Actor.h"
#include "Structs/CBFloorData.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "CBChunk.generated.h"

class UCBPathfinding;

UCLASS()
class CAPTAINBATTLE_API ACBChunk : public AActor
{
	GENERATED_BODY()
	
public:
	ACBChunk();

protected:
	virtual void BeginPlay() override;
	
	virtual void Tick(float DeltaTime) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
public:

	UFUNCTION(Client, Reliable)
	void Client_RenderMap();
	
	const int32 ChunkSize = 20;

	UPROPERTY(BlueprintReadWrite, Replicated)
	FIntPoint ChunkIndex = {};

	UPROPERTY(BlueprintReadWrite)
	ACBWorldManager* WorldManager = nullptr;

	UPROPERTY(BlueprintReadWrite)
	TArray<FCBFloorData> FloorsData = {};
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TMap<TEnumAsByte<ECBTileType>,UHierarchicalInstancedStaticMeshComponent*> Floors = {};

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UHierarchicalInstancedStaticMeshComponent* Grid = nullptr;

	UPROPERTY(ReplicatedUsing=OnRep_Tiles)
	TArray<TEnumAsByte<ECBTileType>> Tiles = {};

	UFUNCTION()
	void OnRep_Tiles();
	
	UPROPERTY(BlueprintReadWrite)
	TMap<int32,FIntPoint> InstanceIndexes = {};
	TMap<FIntPoint,int32> TilesInstances = {};
};
