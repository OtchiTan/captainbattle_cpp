﻿#pragma once

#include "Core/Level/Enums/CBTileType.h"
#include "Core/Level/Enums/CBTravelable.h"
#include "Core/Player/Enums/CBResourceType.h"
#include "Engine/DataTable.h"
#include "CBFloorData.generated.h"

USTRUCT(BlueprintType)
struct CAPTAINBATTLE_API FCBFloorData : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBTileType> Type;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName Name;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CostToEnterTile = 1;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float MinNoise;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float MaxNoise;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float TileHeight;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UStaticMesh* StaticMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UMaterialInstance* MeshMaterial;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	bool bIsSpawnable;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBTravelable> Travelable;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBResourceType> SpawnableResource = RT_None;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int32 MinResource = 0;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int32 MaxResource = 5;
};
