#pragma once

#include "CoreMinimal.h"
#include "Core/Level/Enums/CBTileType.h"
#include "CBTile.generated.h"

class ACBChunk;
class ACBPawn;

USTRUCT(BlueprintType)
struct FCBTile
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FIntPoint Index = {-1,-1};

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<ECBTileType> Type = TT_None;

	UPROPERTY(BlueprintReadWrite)
	ACBPawn* Occupant = nullptr;

	FCBTile()	{ }

	FCBTile(const FIntPoint InIndex, const ECBTileType InType, ACBPawn* InOccupant = nullptr)
	{
		Index = InIndex;
		Type = InType;
		Occupant = InOccupant;
	}
};