#pragma once

#include "Core/Level/Enums/CBHighlightType.h"
#include "Engine/DataTable.h"
#include "CBHighlightedTile.generated.h"

USTRUCT(BlueprintType)
struct CAPTAINBATTLE_API FCBHighlightedTile : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBHighlightType> HighlightType;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName Name;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FColor Color;
};
