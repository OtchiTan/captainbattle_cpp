#pragma once

#include "CoreMinimal.h"
#include "Core/Player/Enums/CBResourceType.h"
#include "CBTileResource.generated.h"

USTRUCT(BlueprintType)
struct FCBTileResource
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<ECBResourceType> ResourceType = RT_None;

	UPROPERTY(BlueprintReadWrite)
	int32 Quantity = 0;

	FCBTileResource() {}

	FCBTileResource(TEnumAsByte<ECBResourceType> InResourceType, int32 InQuantity)
	{
		ResourceType = InResourceType;
		Quantity = InQuantity;
	}
};