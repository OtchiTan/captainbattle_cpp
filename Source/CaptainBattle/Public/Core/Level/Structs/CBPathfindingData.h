#pragma once

#include "CoreMinimal.h"
#include "CBPathfindingData.generated.h"

USTRUCT(BlueprintType)
struct FCBPathfindingData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FIntPoint Index = {-999,-999};
	
	UPROPERTY(BlueprintReadWrite)
	int32 CostToEnterTile = 1;
	
	UPROPERTY(BlueprintReadWrite)
	int32 CostFromStart = 999999;
	
	UPROPERTY(BlueprintReadWrite)
	int32 MinimumCostToTarget = 999999;
	
	UPROPERTY(BlueprintReadWrite)
	FIntPoint PreviousIndex = {-999,-999};

	FCBPathfindingData() {}
	
	FCBPathfindingData(FIntPoint InIndex, FIntPoint InPreviousIndex) : Index(InIndex), PreviousIndex(InPreviousIndex) {};
	
	FCBPathfindingData(FIntPoint InIndex, int32 InCostToEnterTile, int32 InCostFromStart, int32 InMinimumCostToTarget, FIntPoint InPreviousIndex )
		: Index(InIndex), CostToEnterTile(InCostToEnterTile), CostFromStart(InCostFromStart), MinimumCostToTarget(InMinimumCostToTarget), PreviousIndex(InPreviousIndex) { }
};
