﻿#pragma once

#include "CBTileResource.h"
#include "Core/Level/Enums/CBTileType.h"

#include "CBChunkData.generated.h"

USTRUCT(BlueprintType)
struct FCBChunkData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FIntPoint ChunkIndex;

	UPROPERTY(BlueprintReadWrite)
	TArray<TEnumAsByte<ECBTileType>> Tiles;

	TMap<FIntPoint,FCBTileResource> Resources;
};
