#pragma once

#include <Core/Construction/Enums/CBBuildType.h>

#include "CoreMinimal.h"
#include "Core/Player/Structs/CBResourceData.h"
#include "GameFramework/Actor.h"
#include "Structs/CBChunkData.h"
#include "Structs/CBFloorData.h"
#include "Structs/CBTile.h"
#include "CBWorldManager.generated.h"

class ACBPlayerController;
class ACBConstruction;
class UCBChunkManager;
class ACBPawn;
class ACBChunk;
class UCBPathfinding;

UCLASS()
class CAPTAINBATTLE_API ACBWorldManager : public AActor
{
	GENERATED_BODY()
	
public:
	ACBWorldManager();

protected:
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;
public:
	virtual void Tick(float DeltaTime) override;
	
	const int32 ChunkSize = 20;

	UPROPERTY(BlueprintReadWrite)
	int32 Seed = 0;

	UPROPERTY(BlueprintReadWrite)
	bool bFirstChunkIsLoad = false;

	bool IsValidIndex(FIntPoint Index);

	UFUNCTION(BlueprintCallable)
	FIntPoint GetChunkIndex(FIntPoint TileIndex);
	
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_AddOccupant(FIntPoint Tile, ACBPawn* Pawn);

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_RemoveOccupant(FIntPoint Tile, ACBPawn* Pawn);

	bool IsOccupiedTile(FIntPoint Tile) const;

	UFUNCTION(BlueprintCallable)
	void GetWorldTileType(FIntPoint WorldIndex, TEnumAsByte<ECBTileType>& TileType, FIntPoint& TileIndex);
	
	UFUNCTION(BlueprintCallable)
	void GetWorldTileData(FIntPoint WorldIndex, FCBFloorData& FloorData);
	
	void GeneratedWorld();
	FCBChunkData GenerateChunkData(FIntPoint ChunkIndex);
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	TArray<FCBFloorData> FloorsData = {};
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Scale = 9;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Octaves = 3;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Lacunarity = 0.75f;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Persistance = 0.5f;
	
	UPROPERTY()
	TMap<FIntPoint,ACBPawn*> Occupants = {};

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FVector GetTileLocation(FIntPoint Index);

	TArray<FIntPoint> SpawnableTiles = {};

	TMap<FIntPoint,FCBChunkData> GeneratedChunks = {};

	UFUNCTION(BlueprintCallable,BlueprintPure)
	void GetResourceData(const FIntPoint& WorldIndex, FCBTileResource& Data);

	void SetResourceData(const FIntPoint& WorldIndex, const FCBTileResource& Data);
	
	bool GatherResources(const FIntPoint& WorldIndex, FCBTileResource& Data);
	
	UPROPERTY(BlueprintReadWrite)
	UCBPathfinding* Pathfinding = nullptr;

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_BuildConstruction(const FIntPoint& WorldIndex, ECBBuildType Type, ACBPawn* Pawn);

	UPROPERTY(BlueprintReadOnly)
	TMap<FIntPoint,ACBConstruction*> Constructions;

};
