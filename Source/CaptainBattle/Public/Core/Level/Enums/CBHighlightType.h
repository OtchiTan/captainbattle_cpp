﻿#pragma once

#include "CBHighlightType.generated.h"

UENUM()
enum ECBHighlightType
{
	HT_None					UMETA(DisplayName = "None"),
	HT_Reachable			UMETA(DisplayName = "Reachable"),
	HT_Path					UMETA(DisplayName = "Path"),
	HT_Selected				UMETA(DisplayName = "Selected"),
	HT_Constructable		UMETA(DisplayName = "Constructable")
};
