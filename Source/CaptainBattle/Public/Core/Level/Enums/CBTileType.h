#pragma once

#include "CoreMinimal.h"
#include "CBTileType.generated.h"

UENUM(BlueprintType)
enum ECBTileType
{	
	TT_None			UMETA(DisplayName = "None"),
	TT_Ocean		UMETA(DisplayName = "Ocean"),
	TT_Sea			UMETA(DisplayName = "Sea"),
	TT_Sand			UMETA(DisplayName = "Sand"),
	TT_Grass		UMETA(DisplayName = "Grass"),
	TT_Forest		UMETA(DisplayName = "Forest"),
	TT_Mountain		UMETA(DisplayName = "Mountain"),
};