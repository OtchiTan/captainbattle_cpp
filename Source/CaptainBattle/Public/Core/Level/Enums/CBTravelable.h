#pragma once

#include "CoreMinimal.h"
#include "CBTravelable.generated.h"

UENUM(BlueprintType)
enum ECBTravelable
{	
	T_None				UMETA(DisplayName = "None"),
	T_Water				UMETA(DisplayName = "Water"),
	T_Ground			UMETA(DisplayName = "Ground"),
	T_Both				UMETA(DisplayName = "Both"),
};