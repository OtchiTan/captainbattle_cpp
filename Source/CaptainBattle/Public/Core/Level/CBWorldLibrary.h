#pragma once

#include "CoreMinimal.h"
#include "Enums/CBTravelable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CBWorldLibrary.generated.h"

UCLASS()
class CAPTAINBATTLE_API UCBWorldLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static float EvaluateHeight(FIntPoint Index, float Scale, int32 Octaves, float Lacunarity, float Persistance, float MaxPossibleHeight);

	static int32 GetTileIndex(FIntPoint Index);

	static FVector IndexToWorldLocation(const FIntPoint& Index);
	
	static void SetSeed(int seed);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static bool IsTravelable(ECBTravelable Traveler, ECBTravelable Target);
};
