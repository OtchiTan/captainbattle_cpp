#pragma once

#include "CoreMinimal.h"
#include "Enums/CBPlayState.h"
#include "GameFramework/PlayerState.h"
#include "CBPlayerState.generated.h"

UCLASS()
class CAPTAINBATTLE_API ACBPlayerState : public APlayerState
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	UFUNCTION(Server,Reliable)
	void Server_Init();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;
		
public:
	UPROPERTY(ReplicatedUsing=OnRep_State)
	TEnumAsByte<ECBPlayState> State = PS_Waiting;

	UFUNCTION()
	void OnRep_State();
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnStateChange();

	UFUNCTION(BlueprintCallable)
	void SetState(ECBPlayState PlayState);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	ECBPlayState GetState();
};
