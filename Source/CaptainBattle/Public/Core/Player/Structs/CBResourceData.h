#pragma once

#include "Core/Player/Enums/CBResourceType.h"
#include "Engine/DataTable.h"
#include "CBResourceData.generated.h"

USTRUCT(BlueprintType)
struct CAPTAINBATTLE_API FCBResourceData : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TEnumAsByte<ECBResourceType> Type;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName Name;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UTexture2D* Texture;
};
