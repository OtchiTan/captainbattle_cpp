#pragma once

#include "CoreMinimal.h"
#include "CBPlayState.generated.h"

UENUM(BlueprintType)
enum ECBPlayState
{	
	PS_Playing		UMETA(DisplayName = "Playing"),
	PS_Waiting		UMETA(DisplayName = "Waiting"),
};