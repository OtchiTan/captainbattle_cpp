#pragma once

#include "CoreMinimal.h"
#include "CBResourceType.generated.h"

UENUM(BlueprintType)
enum ECBResourceType
{	
	RT_None				UMETA(DisplayName = "None"),
	RT_Wood				UMETA(DisplayName = "Wood"),
	RT_Stone			UMETA(DisplayName = "Stone"),
	RT_Cannonball		UMETA(DisplayName = "Cannonball"),
	RT_Rhum				UMETA(DisplayName = "Rhum"),
};