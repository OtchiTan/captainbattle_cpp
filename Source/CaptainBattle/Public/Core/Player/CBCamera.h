#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "CBCamera.generated.h"

UCLASS()
class CAPTAINBATTLE_API ACBCamera : public APawn
{
	GENERATED_BODY()

public:
	ACBCamera();

protected:
	virtual void BeginPlay() override;

	void Zoom(float Value);
	void Forward(float Value);
	void Right(float Value);
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UPROPERTY()
	UCameraComponent* Camera;

	UPROPERTY()
	USpringArmComponent* SpringArm;

	UPROPERTY(BlueprintReadWrite)
	float ZoomDesired = 500;

	UPROPERTY(BlueprintReadWrite)
	FVector LocationDesired = { 0,0,100 };

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	float MinZoom = 200;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	float MaxZoom = 5000;
};
