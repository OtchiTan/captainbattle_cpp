#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CBGameMode.generated.h"

UCLASS()
class CAPTAINBATTLE_API ACBGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
	ACBGameMode();

};
