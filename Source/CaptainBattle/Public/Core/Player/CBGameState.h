#pragma once

#include "CoreMinimal.h"
#include "Core/Level/CBWorldManager.h"
#include "GameFramework/GameStateBase.h"
#include "CBGameState.generated.h"

class ACBChunk;
class UGrid;
class ACBPlayerController;

UCLASS()
class CAPTAINBATTLE_API ACBGameState : public AGameStateBase
{
	GENERATED_BODY()

	ACBGameState();

	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;
	
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 Seed = 0;

	UPROPERTY(BlueprintReadWrite, Replicated)
	ACBWorldManager* WorldManager = nullptr;

	UFUNCTION(Server, Reliable)
	void Server_SpawnWorld();

	UFUNCTION(Server,Reliable)
	void Server_SpawnPlayer(ACBPlayerController* PlayerController);

	UFUNCTION(Server,Reliable)
	void Server_SpawnPlayers();

	UFUNCTION(Server,Reliable)
	void Server_FinishRound();

	void FindSpawn();

	TArray<FIntPoint> UsedSpawns = {};

	TArray<ACBPlayerController*> PlayersToSpawn = {};

	UPROPERTY(EditAnywhere)
	TSubclassOf<ACBPawn> PlayerClass = nullptr;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ACBWorldManager> WorldManagerClass = nullptr;

	FIntPoint SpawnIndex = -1;

	UPROPERTY(Replicated,BlueprintReadWrite)
	int32 Round = 1;

	int32 PlayerIndex = 0;
};
