#pragma once

#include "CoreMinimal.h"
#include "CBGameState.h"
#include "Core/Level/CBWorldManager.h"
#include "Core/Level/Structs/CBTile.h"
#include "Core/Pawn/CBPawn.h"
#include "GameFramework/PlayerController.h"
#include "CBPlayerController.generated.h"

class UCBChunkManager;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDataChange);

UCLASS()
class CAPTAINBATTLE_API ACBPlayerController : public APlayerController
{
	GENERATED_BODY()

	ACBPlayerController();

	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UCBChunkManager* ChunkManager;
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_MainPawn)
	ACBPawn* MainPawn;

	UFUNCTION()
	void OnRep_MainPawn();

	UPROPERTY(BlueprintReadWrite)
	ACBWorldManager* WorldManager;

	UPROPERTY()
	ACBGameState* GameState;

	UFUNCTION(Client,Reliable)
	void Client_InitCamera();

	UFUNCTION(Server,Reliable)
	void Server_SpawnPlayer();

	UPROPERTY(BlueprintReadWrite, Replicated)
	TArray<ACBPawn*> ControlledPawns;

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_GetTile(FIntPoint WorldIndex);

	UFUNCTION(Client,Reliable)
	void Client_SetSelectedTile(FCBTile Tile, FCBTileResource TileResource);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowData();

	UFUNCTION(BlueprintCallable,Client,Reliable)
	void Client_FinishRound();

	UFUNCTION(Server,Reliable)
	void Server_FinishRound();

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void Server_GatherResource(const FIntPoint& WorldIndex);

	UFUNCTION(Client, Reliable)
	void Client_GatherResource(const FCBTileResource& TileResource);

	UPROPERTY(BlueprintReadWrite)
	FCBTile SelectedTile;

	UPROPERTY(BlueprintReadWrite)
	bool bSelectTile = false;

	UPROPERTY(BlueprintAssignable)
	FOnDataChange OnDataChange;

	UFUNCTION(BlueprintCallable)
	void DEBUG_LASTCHANCE();
};
