﻿#include "Core/CBGameInstance.h"

UCBGameInstance::UCBGameInstance()
{
	const ConstructorHelpers::FObjectFinder<UDataTable> FloorTableFinder(TEXT("DataTable'/Game/Blueprint/Core/Level/DT_Floors.DT_Floors'"));
	if (FloorTableFinder.Object)
	{
		FloorTable = FloorTableFinder.Object;
		
		TArray<FName> RowNames = FloorTable->GetRowNames();
		FloorsData.Add(FCBFloorData());

		for (const FName& Name : RowNames)
		{
			FString ContextString;
			if (const FCBFloorData* Floor = FloorTable->FindRow<FCBFloorData>(Name, ContextString))
			{
				FloorsData.Add(*Floor);
			}
		}
	}

	const ConstructorHelpers::FObjectFinder<UDataTable> ResourceTableFinder(TEXT("DataTable'/Game/Blueprint/Core/Player/DT_Resource.DT_Resource'"));
	if (ResourceTableFinder.Object)
	{
		ResourceTable = ResourceTableFinder.Object;
		
		TArray<FName> RowNames = ResourceTable->GetRowNames();
		ResourceData.Add(FCBResourceData());

		for (const FName& Name : RowNames)
		{
			FString ContextString;
			if (const FCBResourceData* Resource = ResourceTable->FindRow<FCBResourceData>(Name, ContextString))
			{
				ResourceData.Add(*Resource);
			}
		}
	}
	
	const ConstructorHelpers::FObjectFinder<UDataTable> BuildTableFinder(TEXT("DataTable'/Game/Blueprint/Core/Construction/DT_Builds.DT_Builds'"));
	if (BuildTableFinder.Object)
	{
		BuildTable = BuildTableFinder.Object;
		
		TArray<FName> RowNames = BuildTable->GetRowNames();
		BuildData.Add(FCBBuildData());

		for (const FName& Name : RowNames)
		{
			FString ContextString;
			if (const FCBBuildData* Resource = BuildTable->FindRow<FCBBuildData>(Name, ContextString))
			{
				BuildData.Add(*Resource);
			}
		}
	}
	
	const ConstructorHelpers::FObjectFinder<UDataTable> HighlightedTileFinder(TEXT("DataTable'/Game/Blueprint/Core/Level/DT_HighlightedTile.DT_HighlightedTile'"));
	if (HighlightedTileFinder.Object)
	{
		HighlightedTileTable = HighlightedTileFinder.Object;
		
		TArray<FName> RowNames = HighlightedTileTable->GetRowNames();
		HighlightedTileData.Add(FCBHighlightedTile());

		for (const FName& Name : RowNames)
		{
			FString ContextString;
			if (const FCBHighlightedTile* HighlightedTile = HighlightedTileTable->FindRow<FCBHighlightedTile>(Name, ContextString))
			{
				HighlightedTileData.Add(*HighlightedTile);
			}
		}
	}
}

FCBResourceData UCBGameInstance::GetResourceData(const ECBResourceType& Type) const
{
	return ResourceData[Type];
}

FCBBuildData UCBGameInstance::GetBuildData(const ECBBuildType& Type) const
{
	return BuildData[Type];
}

FCBHighlightedTile UCBGameInstance::GetHighlightedTile(const ECBHighlightType& Type) const
{
	return HighlightedTileData[Type];
}
