#include "Core/Level/CBChunkManager.h"

#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Core/CBGameInstance.h"
#include "Core/Level/CBChunk.h"
#include "Core/Level/CBWorldLibrary.h"
#include "Core/Player/CBPlayerController.h"
#include "Kismet/GameplayStatics.h"

UCBChunkManager::UCBChunkManager()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCBChunkManager::BeginPlay()
{
	Super::BeginPlay();

	for (int i = HT_Reachable; i != HT_Selected; ++i)
	{
		HighlightedTiles.Add(static_cast<ECBHighlightType>(i), {});
	}
}

void UCBChunkManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	
}

void UCBChunkManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UCBChunkManager::Client_RecomputeChunks_Implementation(FIntPoint NewOccupiedChunk)
{
	OccupiedChunk = NewOccupiedChunk;
	TMap<FIntPoint,ACBChunk*> ChunksToMove = TMap(LoadedChunks);
	TArray<FIntPoint> LocationsNeedChunks;

	for (int ChunkX = OccupiedChunk.X - 1; ChunkX <= OccupiedChunk.X + 1; ++ChunkX)
	{
		for (int ChunkY = OccupiedChunk.Y - 1; ChunkY <= OccupiedChunk.Y + 1; ++ChunkY)
		{
			const FIntPoint ChunkIndex = FIntPoint(ChunkX,ChunkY);
			if (LoadedChunks.Contains(ChunkIndex))
				ChunksToMove.Remove(ChunkIndex);
			else
				LocationsNeedChunks.Add(ChunkIndex);
		}
	}

	int i = 0;
	for (const TPair<FIntPoint,ACBChunk*>& Chunk : ChunksToMove)
	{
		Server_MoveChunk(Chunk.Value,LocationsNeedChunks[i]);
		i++;
	}
}

void UCBChunkManager::Server_MoveChunk_Implementation(ACBChunk* Chunk, FIntPoint NewLocation)
{
	Chunk->SetActorLocation(FVector(NewLocation.X,NewLocation.Y * 2,0) * ChunkSize * FVector(150,100,0));
	LoadedChunks.Remove(Chunk->ChunkIndex);
	LoadedChunks.Add(NewLocation,Chunk);
	Chunk->ChunkIndex = NewLocation;
	if (const FCBChunkData* ChunkData = WorldManager->GeneratedChunks.Find(NewLocation))
		Chunk->Tiles = ChunkData->Tiles;
	else
		Chunk->Tiles = WorldManager->GenerateChunkData(NewLocation).Tiles;

	if (GetNetMode() == NM_ListenServer)
	{
		Chunk->Client_RenderMap();
	}
}

void UCBChunkManager::Client_SetWorldManager_Implementation()
{
	WorldManager = Cast<ACBWorldManager>(UGameplayStatics::GetActorOfClass(GetWorld(),ACBWorldManager::StaticClass()));
}

void UCBChunkManager::Client_SpawnChunks_Implementation()
{
	Server_SpawnChunks();
}

void UCBChunkManager::Server_SpawnChunks_Implementation()
{
	WorldManager = Cast<ACBWorldManager>(UGameplayStatics::GetActorOfClass(GetWorld(),ACBWorldManager::StaticClass()));
	Client_SetWorldManager();
	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			FCBChunkData ChunkData = WorldManager->GeneratedChunks.FindRef(FIntPoint(x,y));
			FTransform ChunkPos = FTransform(FRotator(0),FVector(x,y * 2,0) * ChunkSize * FVector(150,100,0),FVector(1));
			ACBChunk* Chunk = GetWorld()->SpawnActorDeferred<ACBChunk>(ChunkClass,ChunkPos,
				GetOwner(),nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);			
			Chunk->WorldManager = WorldManager;
			Chunk->ChunkIndex = ChunkData.ChunkIndex;
			Chunk->Tiles = ChunkData.Tiles;
			Chunk->FinishSpawning(ChunkPos);
			LoadedChunks.Add(ChunkData.ChunkIndex,Chunk);
		}

	Cast<ACBPlayerController>(GetOwner())->Server_SpawnPlayer();
}

void UCBChunkManager::Client_HighlightTile_Implementation(FIntPoint WorldIndex, ECBHighlightType HighlightType)
{
	const FIntPoint ChunkIndex = WorldManager->GetChunkIndex(WorldIndex);
	
	if (const ACBChunk* Chunk = LoadedChunks.FindRef(ChunkIndex))
	{
		const FIntPoint TileIndex = WorldIndex - FIntPoint(ChunkIndex.X * ChunkSize,ChunkIndex.Y * ChunkSize * 2);
		const UCBGameInstance* GameInstance = Cast<UCBGameInstance>(GetWorld()->GetGameInstance());
		const FCBHighlightedTile HighlightedTile = GameInstance->GetHighlightedTile(HighlightType);
		const int32 Index = UCBWorldLibrary::GetTileIndex(TileIndex);
		Chunk->Grid->SetCustomDataValue(Index,1,HighlightType,true);
		Chunk->Grid->SetCustomDataValue(Index,2,HighlightedTile.Color.R / 255.0f,true);
		Chunk->Grid->SetCustomDataValue(Index,3,HighlightedTile.Color.G / 255.0f,true);
		Chunk->Grid->SetCustomDataValue(Index,4,HighlightedTile.Color.B / 255.0f,true);
		if (HighlightType != HT_None)
		{
			TSet<FIntPoint> TempHighlightedTiles = HighlightedTiles.FindRef(HighlightType);
			TempHighlightedTiles.Add(WorldIndex);
			HighlightedTiles.Add(HighlightType,TempHighlightedTiles);
		}
	}
}

void UCBChunkManager::Client_SelectTile_Implementation(FIntPoint WorldIndex)
{
	Client_ClearHighlightedTiles(HT_Selected);

	Client_HighlightTile(WorldIndex,HT_Selected);
}

void UCBChunkManager::Client_ClearHighlightedTiles_Implementation(ECBHighlightType HighlightType)
{

	if (HighlightType == HT_None)
	{
		for (TPair<ECBHighlightType,TSet<FIntPoint>>& Pair : HighlightedTiles)
		{
			for (const FIntPoint& Tile : Pair.Value)
			{		
				Client_HighlightTile(Tile,HT_None);
			}
			Pair.Value = {};
		}
	}
	else
	{
		for (const FIntPoint& Tile : HighlightedTiles.FindRef(HighlightType))
		{
			ECBHighlightType NewHighlight = HT_None;
			for (const TPair<ECBHighlightType,TSet<FIntPoint>>& Pair : HighlightedTiles)
			{
				if (Pair.Key != HighlightType &&  Pair.Value.Contains(Tile))
				{
					NewHighlight = Pair.Key;
					break;
				}
			}
		
			Client_HighlightTile(Tile,NewHighlight);
		}
		HighlightedTiles.Add(HighlightType,{});
	}
}