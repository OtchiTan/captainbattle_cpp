#include "CaptainBattle/Public/Core/Level/CBChunk.h"

#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Core/CBGameInstance.h"
#include "Core/Level/CBChunkManager.h"
#include "Core/Level/CBWorldLibrary.h"
#include "Net/UnrealNetwork.h"

ACBChunk::ACBChunk()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	bOnlyRelevantToOwner = true;
	
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Grid = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("Grid"));
	Grid->SetupAttachment(RootComponent);
	Grid->SetCollisionResponseToChannel(ECC_EngineTraceChannel1,ECR_Block);
	Grid->NumCustomDataFloats = 5;
}

void ACBChunk::BeginPlay()
{
	Super::BeginPlay();

	SetReplicateMovement(true);
	
	FloorsData = Cast<UCBGameInstance>(GetGameInstance())->FloorsData;
	Floors.Empty();
	
	for (const FCBFloorData& Floor : FloorsData)
	{		
		UHierarchicalInstancedStaticMeshComponent* Hism
			= NewObject<UHierarchicalInstancedStaticMeshComponent>(this,Floor.Name);
		Hism->SetupAttachment(RootComponent);
		Hism->RegisterComponent();
		Hism->SetStaticMesh(Floor.StaticMesh);
		Hism->SetMaterial(0,Floor.MeshMaterial);
		Floors.Add(Floor.Type,Hism);
	}

	Client_RenderMap();
}

void ACBChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACBChunk::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACBChunk,ChunkIndex);
	DOREPLIFETIME(ACBChunk,Tiles);
}

void ACBChunk::OnRep_Tiles()
{
	if (!Tiles.IsEmpty() && Floors.FindRef(TT_Ocean))
		Client_RenderMap();
}

void ACBChunk::Client_RenderMap_Implementation()
{
	Cast<ACBPlayerController>(GetOwner())->ChunkManager->LoadedChunks.Add(ChunkIndex,this);
	
	for (const TPair<TEnumAsByte<ECBTileType>, UHierarchicalInstancedStaticMeshComponent*>& Pair : Floors)
	{
		Pair.Value->ClearInstances();
	}

	Grid->ClearInstances();

	int TileIndex = 0;
	for (int x = 0; x < ChunkSize; ++x)
	{
		const int32 StartY = x % 2 ? 1 : 0;
		const int32 GlobalX = ChunkIndex.X * ChunkSize + x;
		
		for (int y = StartY; y < ChunkSize * 2; y += 2)
		{
			const int32 GlobalY = ChunkIndex.Y * ChunkSize * 2 + y;

			const FCBFloorData FloorData = FloorsData[Tiles[TileIndex]];

			UHierarchicalInstancedStaticMeshComponent* Floor = Floors.FindRef(FloorData.Type);
			
			FVector TileLocation = FVector(x * 150, y * 100, FloorData.TileHeight);
			
			Floor->AddInstance(FTransform(FRotator(0),TileLocation,FVector(2,2,1)));
			
			const int32 InstanceIndex = Grid->AddInstance(FTransform(FRotator(0),TileLocation + FVector::UpVector,FVector(2)));
			Grid->SetCustomDataValue(InstanceIndex,0,UCBWorldLibrary::IsTravelable(T_Water,FloorData.Travelable) ? 0 : 1);

			InstanceIndexes.Add(InstanceIndex,FIntPoint(GlobalX,GlobalY));
			TilesInstances.Add(FIntPoint(GlobalX,GlobalY),InstanceIndex);

			TileIndex++;
		}
	}
}
