// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Level/CBConstruction.h"

// Sets default values
ACBConstruction::ACBConstruction()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACBConstruction::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACBConstruction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

