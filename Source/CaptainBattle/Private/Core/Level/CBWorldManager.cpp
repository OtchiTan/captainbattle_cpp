#include "Core/Level/CBWorldManager.h"

#include "Core/CBGameInstance.h"
#include "Core/Level/CBChunk.h"
#include "Core/Level/CBChunkManager.h"
#include "Core/Level/CBConstruction.h"
#include "Core/Level/CBPathfinding.h"
#include "Core/Level/CBWorldLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ACBWorldManager::ACBWorldManager()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
		
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Pathfinding = CreateDefaultSubobject<UCBPathfinding>(TEXT("Pathfinding"));
}

void ACBWorldManager::BeginPlay()
{
	Super::BeginPlay();

	FloorsData = Cast<UCBGameInstance>(GetGameInstance())->FloorsData;

	const ACBPlayerController* PlayerController = Cast<ACBPlayerController>(
		UGameplayStatics::GetPlayerController(GetWorld(),0)
		);
	
	GeneratedWorld();
	
	
	PlayerController->ChunkManager->Client_SpawnChunks();
}

void ACBWorldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACBWorldManager::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	Pathfinding->WorldManager = this;
}

bool ACBWorldManager::IsValidIndex(FIntPoint Index)
{	
	TEnumAsByte<ECBTileType> TileType;
	FIntPoint TileIndex;
	GetWorldTileType(Index,TileType,TileIndex);
	return TileType != TT_None;
}

FVector ACBWorldManager::GetTileLocation(FIntPoint Index)
{
	TEnumAsByte<ECBTileType> TileType;
	FIntPoint TileIndex;
	GetWorldTileType(Index,TileType,TileIndex);
	if (TileType != TT_None)
	{
		const FCBFloorData FloorData = Cast<UCBGameInstance>(GetGameInstance())->FloorsData[TileType];
		return FVector(150,100,FloorData.TileHeight) * FVector(Index.X,Index.Y,1);
	}
	else return FVector(-1);
}

void ACBWorldManager::GetResourceData(const FIntPoint& WorldIndex, FCBTileResource& Data)
{
	const FIntPoint ChunkIndex = GetChunkIndex(WorldIndex);
	
	if (const FCBChunkData* ChunkData = GeneratedChunks.Find(ChunkIndex))
	{
		const FIntPoint TileIndex = WorldIndex - FIntPoint(ChunkIndex.X * ChunkSize,ChunkIndex.Y * ChunkSize * 2);
		Data = ChunkData->Resources.FindRef(TileIndex);
	}
}

void ACBWorldManager::SetResourceData(const FIntPoint& WorldIndex, const FCBTileResource& Data)
{
	const FIntPoint ChunkIndex = GetChunkIndex(WorldIndex);
	
	if (FCBChunkData* ChunkData = GeneratedChunks.Find(ChunkIndex))
	{
		const FIntPoint TileIndex = WorldIndex - FIntPoint(ChunkIndex.X * ChunkSize,ChunkIndex.Y * ChunkSize * 2);
		ChunkData->Resources.Add(TileIndex, Data);
	}
}

bool ACBWorldManager::GatherResources(const FIntPoint& WorldIndex, FCBTileResource& Data)
{
	const FIntPoint ChunkIndex = GetChunkIndex(WorldIndex);
	
	if (FCBChunkData* ChunkData = GeneratedChunks.Find(ChunkIndex))
	{
		const FIntPoint TileIndex = WorldIndex - FIntPoint(ChunkIndex.X * ChunkSize,ChunkIndex.Y * ChunkSize * 2);
		
		ChunkData->Resources.RemoveAndCopyValue(TileIndex,Data);
		
		return  true;
	}

	return false;
}

void ACBWorldManager::Server_BuildConstruction_Implementation(const FIntPoint& WorldIndex, ECBBuildType Type, ACBPawn* Pawn)
{
	const UCBGameInstance* GameInstance = Cast<UCBGameInstance>(GetGameInstance());
	const FCBBuildData BuildData = GameInstance->GetBuildData(Type);

	for (const TPair<TEnumAsByte<ECBResourceType>,int32> Resource : BuildData.QuantitiesToBuild)
	{
		if (const int32* Quantity = Pawn->Resources.Find(Resource.Key))
		{
			if (*Quantity < Resource.Value)
			{
				return;
			}
		}
		else
		{
			return;
		}
	}
	
	for (const TPair<TEnumAsByte<ECBResourceType>,int32> Resource : BuildData.QuantitiesToBuild)
	{
		Pawn->RemoveResources(FCBTileResource(Resource.Key, Resource.Value));
	}
	
	FCBFloorData FloorData;
	GetWorldTileData(WorldIndex, FloorData);
	
	const FVector ConstructionLocation = UCBWorldLibrary::IndexToWorldLocation(WorldIndex);
	FTransform SpawnTransform = FTransform();
	SpawnTransform.SetLocation(ConstructionLocation + FVector::UpVector * FloorData.TileHeight);
	
	ACBConstruction* Construction = GetWorld()->SpawnActorDeferred<ACBConstruction>(BuildData.Construction, SpawnTransform, this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	Construction->BuildData = BuildData;
	Construction->FinishSpawning(SpawnTransform);

	Constructions.Add(WorldIndex, Construction);
}

void ACBWorldManager::Server_AddOccupant_Implementation(FIntPoint Tile, ACBPawn* Pawn)
{
	if (!IsOccupiedTile(Tile))
		Occupants.Add(Tile,Pawn);
}

void ACBWorldManager::Server_RemoveOccupant_Implementation(FIntPoint Tile, ACBPawn* Pawn)
{
	if (IsOccupiedTile(Tile))
		Occupants.Remove(Tile);
}

bool ACBWorldManager::IsOccupiedTile(FIntPoint Tile) const
{
	return Occupants.Contains(Tile);
}

void ACBWorldManager::GetWorldTileType(FIntPoint WorldIndex, TEnumAsByte<ECBTileType>& TileType, FIntPoint& TileIndex)
{
	const FIntPoint ChunkIndex = GetChunkIndex(WorldIndex);
	
	if (FCBChunkData* ChunkData = GeneratedChunks.Find(ChunkIndex))
	{
		TileIndex = WorldIndex - FIntPoint(ChunkIndex.X * ChunkSize,ChunkIndex.Y * ChunkSize * 2);
		const int32 FlatIndex = UCBWorldLibrary::GetTileIndex(TileIndex);
		TileType = ChunkData->Tiles[FlatIndex];
	}
}

void ACBWorldManager::GetWorldTileData(FIntPoint WorldIndex, FCBFloorData& FloorData)
{
	TEnumAsByte<ECBTileType> TileType;
	FIntPoint TileIndex;
	GetWorldTileType(WorldIndex,TileType,TileIndex);
	FloorData = Cast<UCBGameInstance>(GetOwner()->GetGameInstance())->FloorsData[TileType];
}

void ACBWorldManager::GeneratedWorld()
{	
	for (int ChunkX = -1; ChunkX <= 1; ++ChunkX)
	{
		for (int ChunkY = -1; ChunkY <= 1; ++ChunkY)
		{
			const FIntPoint ChunkIndex = FIntPoint(ChunkX,ChunkY);
			GenerateChunkData(ChunkIndex);
		}
	}
}

FCBChunkData ACBWorldManager::GenerateChunkData(FIntPoint ChunkIndex)
{
	float MaxPossibleHeight = 0.1f;
	float Amplitude = 1;
	for (int o = 0; o < Octaves; ++o)
	{
		MaxPossibleHeight += Amplitude;
		Amplitude *= Persistance;
	}
	
	FCBChunkData ChunkData;
	ChunkData.ChunkIndex = ChunkIndex;
	ChunkData.Tiles.Reserve(ChunkSize * ChunkSize * 2);

	for (int TileX = 0; TileX < ChunkSize; ++TileX)
	{
		const int32 StartY = TileX % 2 ? 1 : 0;
		const int32 GlobalX = ChunkIndex.X * ChunkSize + TileX;
		
		for (int TileY = StartY; TileY < ChunkSize * 2; TileY += 2)
		{
			const int32 GlobalY = ChunkIndex.Y * ChunkSize * 2 + TileY;
					
			const float PerlinNoise = UCBWorldLibrary::EvaluateHeight(FIntPoint(GlobalX,GlobalY),Scale,Octaves,Lacunarity,Persistance, MaxPossibleHeight);

			TArray<FCBFloorData> PossibleFloor;
			
			for (const FCBFloorData& FloorData : FloorsData)
			{
				if (PerlinNoise >= FloorData.MinNoise && PerlinNoise <= FloorData.MaxNoise)
				{
					PossibleFloor.Add(FloorData);
					if (ChunkIndex == FIntPoint::ZeroValue && FloorData.bIsSpawnable && HasAuthority())
						SpawnableTiles.Add(FIntPoint(GlobalX,GlobalY));
				}
			}

			const FCBFloorData FloorData = PossibleFloor[
				UKismetMathLibrary::RandomIntegerInRange(0,PossibleFloor.Num() - 1)
				];

			if (GetNetMode() == NM_ListenServer && FloorData.SpawnableResource != RT_None)
			{
				const int32 Quantity = FMath::RandRange(FloorData.MinResource,FloorData.MaxResource);
				if (Quantity > 0)
				{
					ChunkData.Resources.Add(FIntPoint(TileX,TileY),{
						FloorData.SpawnableResource,
						Quantity
					});
				}
			}
			
			ChunkData.Tiles.Add(FloorData.Type);
		}
	}

	GeneratedChunks.Add(ChunkIndex,ChunkData);
	return ChunkData;
}

FIntPoint ACBWorldManager::GetChunkIndex(FIntPoint TileIndex)
{
	if (TileIndex.X < -1)
		TileIndex.X++;
	if (TileIndex.Y < -1)
		TileIndex.Y++;
	
	FIntPoint ChunkIndex = FIntPoint(
		TileIndex.X / ChunkSize,
		TileIndex.Y / (ChunkSize * 2)
	);

	if (TileIndex.X < 0)
		ChunkIndex.X--;
	if (TileIndex.Y < 0)
		ChunkIndex.Y--;
	
	return ChunkIndex;
}
