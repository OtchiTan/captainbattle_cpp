#include "Core/Level/CBPathfinding.h"
#include "Core/Level/CBWorldManager.h"
#include "Core/CBGameInstance.h"
#include "Core/Level/CBConstruction.h"
#include "Core/Level/CBWorldLibrary.h"

UCBPathfinding::UCBPathfinding()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UCBPathfinding::BeginPlay()
{
	Super::BeginPlay();
	
}

void UCBPathfinding::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

TArray<FIntPoint> UCBPathfinding::FindPath(FIntPoint Start, FIntPoint Target, TEnumAsByte<ECBTravelable> Travelable, bool bReturnReachable, int32 PathLength)
{
	StartIndex = Start;
	TargetIndex = Target;
	MaxPathLength = PathLength;
	bReturnReachableTiles = bReturnReachable;
	Traveler = Travelable;

	ClearGeneratedData();

	if (IsInputDataValid())
	{
		const int32 Cost = GetMinimumCostBetweenTwoTiles(StartIndex,TargetIndex);
		DiscoverTile(FCBPathfindingData(StartIndex,1,0,
			Cost,FIntPoint()));

		while (DiscoveredTileIndexes.Num() > 0)
		{
			if (AnalyseNextDiscoveredTile())
			{
				return GeneratePath();
			}
		}

		if (bReturnReachableTiles)
			return AnalysedTileIndexes;
		return {};
	}

	return {};
}

bool UCBPathfinding::IsInputDataValid() const
{
	if (!WorldManager->IsValidIndex(StartIndex))
		return false;

	if (bReturnReachableTiles)
		return true;
	
	if (!WorldManager->IsValidIndex(TargetIndex))
		return false;
	
	if (!IsTravelable(TargetIndex))
		return false;
	
	return true;
}

void UCBPathfinding::DiscoverTile(const FCBPathfindingData& TilePathData)
{
	PathfindingData.Add(TilePathData.Index,TilePathData);

	InsertTileInDiscoveredArray(TilePathData);
}

int32 UCBPathfinding::GetMinimumCostBetweenTwoTiles(FIntPoint Index1, FIntPoint Index2)
{
	FIntPoint Res = Index1 - Index2;
	Res = FIntPoint(FMath::Abs(Res.X),FMath::Abs(Res.Y));
	int32 x = Res.Y - Res.X;
	x = x / 2;
	x = FMath::Max(x,0);
	return  x + Res.X;
}

TArray<FIntPoint> UCBPathfinding::GeneratePath() const
{
	FIntPoint Current = TargetIndex;
	TArray<FIntPoint> InvertedPath = {};
	
	while (Current != StartIndex)
	{
		InvertedPath.Add(Current);
		
		Current = PathfindingData.FindRef(Current).PreviousIndex;
	}

	Algo::Reverse(InvertedPath);

	return InvertedPath;
}

FCBPathfindingData UCBPathfinding::PullCheapestTileOutOfDiscoveredList()
{
	const FIntPoint TileIndex = DiscoveredTileIndexes[0];

	DiscoveredTileSortingCost.RemoveAt(0);
	DiscoveredTileIndexes.RemoveAt(0);

	AnalysedTileIndexes.Add(TileIndex);
	
	return PathfindingData.FindRef(TileIndex);
}

bool UCBPathfinding::AnalyseNextDiscoveredTile()
{
	CurrentDiscoveredTile = PullCheapestTileOutOfDiscoveredList();

	CurrentNeighbors = GetNeighborIndexes(CurrentDiscoveredTile.Index, Traveler);

	while (CurrentNeighbors.Num() > 0)
	{
		if (DiscoverNextNeighbor())
		{
			return true;
		}
	}

	return false;
}

TArray<FCBPathfindingData> UCBPathfinding::GetNeighborIndexes(FIntPoint Index, TEnumAsByte<ECBTravelable> Travelable)
{
	Traveler = Travelable;
	
	TArray Neighbors = {
		FCBPathfindingData(Index + FIntPoint(1,1), Index),
		FCBPathfindingData(Index + FIntPoint(0,2), Index),
		FCBPathfindingData(Index + FIntPoint(-1,1), Index),
		FCBPathfindingData(Index + FIntPoint(-1,-1), Index),
		FCBPathfindingData(Index + FIntPoint(0,-2), Index),
		FCBPathfindingData(Index + FIntPoint(1,-1), Index),
	};

	TArray<FCBPathfindingData> Result = {};

	for (FCBPathfindingData Neighbor : Neighbors)
	{
		if (!WorldManager->IsValidIndex(Neighbor.Index)) continue;

		FCBFloorData FloorData;
		
		if (!IsTravelable(Neighbor.Index,FloorData)) continue;

		Neighbor.CostToEnterTile = FloorData.CostToEnterTile;
		
		Result.Add(Neighbor);
	}

	return Result;
}

bool UCBPathfinding::DiscoverNextNeighbor()
{	
	CurrentNeighbor = CurrentNeighbors[0];

	CurrentNeighbors.RemoveAt(0);

	if (AnalysedTileIndexes.Contains(CurrentNeighbor.Index))
		return false;

	const int32 CostFromStart = CurrentDiscoveredTile.CostFromStart + CurrentNeighbor.CostToEnterTile;

	if (CostFromStart > MaxPathLength)
		return false;

	const int32 IndexInDiscovered = DiscoveredTileIndexes.Find(CurrentNeighbor.Index);

	if (IndexInDiscovered != -1)
	{
		CurrentNeighbor = PathfindingData.FindRef(CurrentNeighbor.Index);
		if (CostFromStart > CurrentNeighbor.CostFromStart)
			return false;
		
		DiscoveredTileIndexes.RemoveAt(IndexInDiscovered);
		DiscoveredTileSortingCost.RemoveAt(IndexInDiscovered);
	}

	DiscoverTile(FCBPathfindingData(CurrentNeighbor.Index,CurrentNeighbor.CostToEnterTile,CostFromStart,
		GetMinimumCostBetweenTwoTiles(CurrentNeighbor.Index,TargetIndex),CurrentDiscoveredTile.Index));

	return CurrentNeighbor.Index == TargetIndex;
}

void UCBPathfinding::InsertTileInDiscoveredArray(const FCBPathfindingData& TileData)
{
	const int32 SortingCost = GetTileSortingCost(TileData);

	if (DiscoveredTileSortingCost.Num() == 0 || SortingCost >= DiscoveredTileSortingCost.Last())
	{
		DiscoveredTileSortingCost.Add(SortingCost);
		DiscoveredTileIndexes.Add(TileData.Index);
	}
	else
	{
		int32 i = 0;
		for (const int32 TileSortingCost : DiscoveredTileSortingCost)
		{
			if (TileSortingCost >= SortingCost)
			{
				DiscoveredTileSortingCost.Insert(SortingCost,i);
				DiscoveredTileIndexes.Insert(TileData.Index,i);

				break;
			}

			i++;
		}
	}
}

void UCBPathfinding::ClearGeneratedData()
{
	PathfindingData.Empty();
	DiscoveredTileIndexes.Empty();
	DiscoveredTileSortingCost.Empty();
	AnalysedTileIndexes.Empty();
}

int32 UCBPathfinding::GetTileSortingCost(const FCBPathfindingData& TileData)
{
	TArray Neighbors = GetNeighborIndexes(TileData.Index, Traveler);
	bool bIsDiagonal = false;

	for (FCBPathfindingData Neighbor : Neighbors)
	{
		if (Neighbor.Index == TileData.Index)
			bIsDiagonal = true;
	}
	
	return !bIsDiagonal + (TileData.CostFromStart + TileData.MinimumCostToTarget) * 2;
}

bool UCBPathfinding::IsTravelable(FIntPoint Index) const
{
	FCBFloorData FloorData;
	return IsTravelable(Index,FloorData);
}

bool UCBPathfinding::IsTravelable(FIntPoint Index, FCBFloorData& FloorData) const
{
	WorldManager->GetWorldTileData(Index,FloorData);

	if (const ACBConstruction* Construction = WorldManager->Constructions.FindRef(Index))
	{
		return UCBWorldLibrary::IsTravelable(Traveler, Construction->BuildData.Travelable);
	}
	
	return UCBWorldLibrary::IsTravelable(Traveler,FloorData.Travelable) && !WorldManager->Occupants.Contains(Index);
}
