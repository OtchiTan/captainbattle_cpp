#include "Core/Level/CBWorldLibrary.h"

float UCBWorldLibrary::EvaluateHeight(FIntPoint Index, float Scale, int32 Octaves, float Lacunarity, float Persistance, float MaxPossibleHeigth)
{
	float Frequency = 1;
	float Amplitude = 1;
	float NoiseHeight = 0;
	
	for (int i = 0; i < Octaves; ++i)
	{
		const float SampleX = Index.X / Scale * Frequency;
		const float SampleY = Index.Y / Scale * Frequency;

		const float PerlinValue = FMath::PerlinNoise2D(FVector2D(SampleX,SampleY));

		NoiseHeight += PerlinValue * Amplitude;
		Amplitude *= Persistance;
		Frequency *= Lacunarity;
	}

	return FMath::Clamp((NoiseHeight + 1) / (2.0f * MaxPossibleHeigth),0,MaxPossibleHeigth);
}

int32 UCBWorldLibrary::GetTileIndex(FIntPoint Index)
{
	return Index.X * 20 + Index.Y / 2;
}

FVector UCBWorldLibrary::IndexToWorldLocation(const FIntPoint& Index)
{
	return FVector(Index.X * 150, Index.Y * 100,0);
}

void UCBWorldLibrary::SetSeed(int seed)
{
	srand(seed);
}

bool UCBWorldLibrary::IsTravelable(ECBTravelable Traveler, ECBTravelable Target)
{
	if (Target == T_None)
	{
		return false;
	}

	if (Target == T_Both)
	{
		return true;
	}

	if (Traveler == Target)
	{
		return true;
	}

	return false;
}
