#include "CaptainBattle/Public/Core/Player/CBCamera.h"

ACBCamera::ACBCamera()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);

	Camera->bAutoActivate = true;
	SpringArm->bDoCollisionTest = false;

	Camera->SetupAttachment(SpringArm);

	SpringArm->SetRelativeRotation(FRotator(-70,0,0));
}

void ACBCamera::BeginPlay()
{
	Super::BeginPlay();
	
	LocationDesired = GetActorLocation();
}

void ACBCamera::Zoom(float Value)
{
	ZoomDesired = FMath::Clamp(ZoomDesired + Value * 40,MinZoom,MaxZoom);
}

void ACBCamera::Forward(float Value)
{
	LocationDesired += GetActorForwardVector() * (Value * 20);
}

void ACBCamera::Right(float Value)
{
	LocationDesired += GetActorRightVector() * (Value * 20);
}

void ACBCamera::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

}

void ACBCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength,ZoomDesired,DeltaTime,2);

	SetActorLocation(FMath::VInterpTo(GetActorLocation(),LocationDesired,DeltaTime,5));
}

void ACBCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("InputAxis_Zoom",this,&ACBCamera::Zoom);

	PlayerInputComponent->BindAxis("InputAxis_Forward", this, &ACBCamera::Forward);
	PlayerInputComponent->BindAxis("InputAxis_Right", this, &ACBCamera::Right);
}