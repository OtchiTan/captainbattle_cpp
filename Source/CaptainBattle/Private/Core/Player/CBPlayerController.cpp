#include "CaptainBattle/Public/Core/Player/CBPlayerController.h"

#include "Core/Level/CBChunkManager.h"
#include "Core/Player/CBCamera.h"
#include "Core/Player/CBGameState.h"
#include "Net/UnrealNetwork.h"

ACBPlayerController::ACBPlayerController() : Super()
{
	bShowMouseCursor = true;

	ChunkManager = CreateDefaultSubobject<UCBChunkManager>(TEXT("ChunkManager"));
}

void ACBPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ACBPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACBPlayerController, MainPawn)
	DOREPLIFETIME(ACBPlayerController, ControlledPawns)
}

void ACBPlayerController::OnRep_MainPawn()
{
	GameState = Cast<ACBGameState>(GetWorld()->GetGameState());
	Client_InitCamera();
}

void ACBPlayerController::Client_GatherResource_Implementation(const FCBTileResource& TileResource)
{
	MainPawn->AddResources(TileResource);
}

void ACBPlayerController::DEBUG_LASTCHANCE()
{
	UE_LOG(LogTemp,Error,TEXT("%i"), WorldManager->Occupants.Num())
}

void ACBPlayerController::Server_GatherResource_Implementation(const FIntPoint& WorldIndex)
{
	FCBTileResource TileResource;
	if (WorldManager->GatherResources(WorldIndex, TileResource))
	{
		MainPawn->AddResources(TileResource);

		Server_GetTile(WorldIndex);

		if (GetRemoteRole() == ROLE_AutonomousProxy)
		{
			Client_GatherResource(TileResource);
		}
	}
}

void ACBPlayerController::Server_FinishRound_Implementation()
{
	Cast<ACBGameState>(GetWorld()->GetGameState())->Server_FinishRound();
}

void ACBPlayerController::Client_FinishRound_Implementation()
{
	Server_FinishRound();
}

void ACBPlayerController::Server_SpawnPlayer_Implementation()
{
	GameState = Cast<ACBGameState>(GetWorld()->GetGameState());
	WorldManager = GameState->WorldManager;
	GameState->Server_SpawnPlayer(this);
}

void ACBPlayerController::Client_InitCamera_Implementation()
{
	WorldManager = GameState->WorldManager;
	ACBCamera* Camera = Cast<ACBCamera>(GetPawn());
	Camera->SetActorLocation(MainPawn->GetActorLocation());
	Camera->LocationDesired = MainPawn->GetActorLocation();
}

void ACBPlayerController::Server_GetTile_Implementation(FIntPoint WorldIndex)
{
	if (WorldManager)
	{
		TEnumAsByte<ECBTileType> TileType;
		FIntPoint TileIndex;
		WorldManager->GetWorldTileType(WorldIndex,TileType,TileIndex);
		if (TileType != TT_None)
		{
			FCBTile Tile;
			Tile.Type = TileType;
			Tile.Occupant = WorldManager->Occupants.FindRef(WorldIndex);
			Tile.Index = WorldIndex;
			FCBTileResource TileResource;
			WorldManager->GetResourceData(WorldIndex, TileResource);
			Client_SetSelectedTile(Tile, TileResource);
		}
	}
}

void ACBPlayerController::Client_SetSelectedTile_Implementation(FCBTile Tile, FCBTileResource TileResource)
{
	SelectedTile = Tile;
	WorldManager->SetResourceData(Tile.Index, TileResource);
	ShowData();
}
