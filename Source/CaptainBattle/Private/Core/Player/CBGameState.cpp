#include "Core/Player/CBGameState.h"

#include "Core/Level/Structs/CBPathfindingData.h"
#include "Core/Pawn/CBGridMovement.h"
#include "Core/Pawn/CBPawn.h"
#include "Core/Level/CBPathfinding.h"
#include "Core/Player/CBPlayerState.h"
#include "Core/Player/CBPlayerController.h"
#include "Net/UnrealNetwork.h"

ACBGameState::ACBGameState()
{
}

void ACBGameState::BeginPlay()
{
	Super::BeginPlay();
	
	if (HasAuthority())
		Server_SpawnWorld();
}

void ACBGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACBGameState, Seed);
	DOREPLIFETIME(ACBGameState, WorldManager);
	DOREPLIFETIME(ACBGameState, Round);
}

void ACBGameState::Server_FinishRound_Implementation()
{
	if (PlayerIndex < PlayerArray.Num() - 1)
		PlayerIndex++;
	else
	{
		PlayerIndex = 0;
		Round++;
	}

	for (int i = 0; i < PlayerArray.Num(); i++)
	{
		ACBPlayerState* PlayerState = Cast<ACBPlayerState>(PlayerArray[i]);
		PlayerState->SetState(PlayerIndex == i ? PS_Playing : PS_Waiting);
		const ACBPlayerController* PlayerController = Cast<ACBPlayerController>(PlayerState->GetPlayerController());
		UCBGridMovement* GridMovement = PlayerController->MainPawn->GridMovement;
		GridMovement->MovementPoint = GridMovement->MovementPointMax;
	}
}

void ACBGameState::FindSpawn()
{
	if (!WorldManager->SpawnableTiles.IsEmpty())
	{
		SpawnIndex = WorldManager->SpawnableTiles[FMath::RandRange(0,WorldManager->SpawnableTiles.Num() - 1)];
		const TArray<FCBPathfindingData> Neighbors = WorldManager->Pathfinding->GetNeighborIndexes(SpawnIndex,T_Ground);
		if (Neighbors.Num() < 2)
			return FindSpawn();
	}
}

void ACBGameState::Server_SpawnPlayers_Implementation()
{
	for (ACBPlayerController* PlayerController : PlayersToSpawn)
	{
		Server_SpawnPlayer(PlayerController);
	}
}

void ACBGameState::Server_SpawnPlayer_Implementation(ACBPlayerController* PlayerController)
{
	if (!WorldManager)
	{
		PlayersToSpawn.Add(PlayerController);
		return;
	}

	if (SpawnIndex == -1)
		FindSpawn();
	
	FIntPoint NewSpawnIndex = SpawnIndex;
	
	if (UsedSpawns.Contains(NewSpawnIndex))
	{
		TArray Neighbors = {
			SpawnIndex + FIntPoint(1,1),
			SpawnIndex + FIntPoint(0,2),
			SpawnIndex + FIntPoint(-1,1),
			SpawnIndex + FIntPoint(-1,-1),
			SpawnIndex + FIntPoint(0,-2),
			SpawnIndex + FIntPoint(1,-1),
		};
		
		for (int i = 0; i < Neighbors.Num(); i++)
		{
			const FIntPoint& Neighbor = Neighbors[FMath::RandRange(0,Neighbors.Num() - 1)];
			if (!UsedSpawns.Contains(Neighbor) && WorldManager->SpawnableTiles.Contains(Neighbor))
			{
				NewSpawnIndex = Neighbor;
				break;
			}
		}
	}

	UsedSpawns.Add(NewSpawnIndex);
	
	FTransform SpawnLocation;
	SpawnLocation.SetLocation(FVector(NewSpawnIndex.X,NewSpawnIndex.Y,40) * FVector(150,100,1));
	
	ACBPawn* Pawn = GetWorld()->SpawnActorDeferred<ACBPawn>(PlayerClass,SpawnLocation,PlayerController,nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	Pawn->GridMovement->WorldManager = WorldManager;
	Pawn->TileIndex = NewSpawnIndex;
	Pawn->FinishSpawning(SpawnLocation);
	Pawn->Name = "Pawn_" + PlayerController->GetName();
	
	PlayerController->MainPawn = Pawn;
	WorldManager->Server_AddOccupant(NewSpawnIndex,Pawn);
	
	if (PlayerController->GetRemoteRole() == ROLE_SimulatedProxy)
	{
		PlayerController->Client_InitCamera();
	}
}

void ACBGameState::Server_SpawnWorld_Implementation()
{	
	WorldManager = GetWorld()->SpawnActorDeferred<ACBWorldManager>(WorldManagerClass,FTransform(),
		this,nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
	WorldManager->Seed = Seed;
	
	WorldManager->FinishSpawning(FTransform());
}
