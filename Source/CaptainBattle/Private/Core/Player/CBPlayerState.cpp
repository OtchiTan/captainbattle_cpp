#include "Core/Player/CBPlayerState.h"

#include "GameFramework/GameStateBase.h"
#include "Net/UnrealNetwork.h"

void ACBPlayerState::BeginPlay()
{
	Super::BeginPlay();
	
	Server_Init();
}

void ACBPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACBPlayerState,State);
}

void ACBPlayerState::OnRep_State()
{
	OnStateChange();
}

void ACBPlayerState::SetState(ECBPlayState PlayState)
{
	State = PlayState;
	OnStateChange();
}

ECBPlayState ACBPlayerState::GetState()
{
	return State;
}

void ACBPlayerState::Server_Init_Implementation()
{
	const AGameStateBase* GameState = GetWorld()->GetGameState();
	SetState(GameState->PlayerArray.Num() == 1 ? PS_Playing : PS_Waiting);
}
