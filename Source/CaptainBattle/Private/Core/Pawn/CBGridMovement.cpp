#include "Core/Pawn/CBGridMovement.h"

#include "Core/Level/CBChunkManager.h"
#include "Core/Pawn/CBPawn.h"
#include "Core/Level/CBWorldManager.h"
#include "Core/Level/CBPathfinding.h"
#include "Core/Player/CBPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

UCBGridMovement::UCBGridMovement()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCBGridMovement::BeginPlay()
{
	Super::BeginPlay();

	SetComponentTickEnabled(false);
}

void UCBGridMovement::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCBGridMovement, Pawn)
	DOREPLIFETIME(UCBGridMovement, PathfindingData);
}

void UCBGridMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Pawn->SetActorLocation(Pawn->GetActorLocation() + MovementProgress * (TargetLocation - Pawn->GetActorLocation()));
	FVector PawnLocation = Pawn->GetActorLocation();
	PawnLocation.X = FMath::RoundToInt(PawnLocation.X);
	PawnLocation.Y = FMath::RoundToInt(PawnLocation.Y);
	PawnLocation.Z = FMath::RoundToInt(PawnLocation.Z);
	MovementProgress += DeltaTime / (MovementSpeed * 10);
	if (PawnLocation == TargetLocation)
		OnReachTile();
}

void UCBGridMovement::OnRep_PathfindingData()
{
	Client_ShowPath();
}

void UCBGridMovement::Client_ShowPath_Implementation()
{
	if (Pawn->GetOwner())
	{
		const ACBPlayerController* PlayerController = Cast<ACBPlayerController>(Pawn->GetOwner());
		if (bShowReachable)
		{
			PlayerController->ChunkManager->Client_ClearHighlightedTiles(HT_Reachable);
			for (const FIntPoint& Tile : PathfindingData)
				PlayerController->ChunkManager->Client_HighlightTile(Tile,HT_Reachable);
		}
		else if (bShowPath)
		{
			PlayerController->ChunkManager->Client_ClearHighlightedTiles(HT_Path);
			for (const FIntPoint& Tile : PathfindingData)
				PlayerController->ChunkManager->Client_HighlightTile(Tile,HT_Path);
		}
	}
}

void UCBGridMovement::Client_FindPath_Implementation(FIntPoint Target, bool ShowReachable)
{
	bShowReachable = ShowReachable;
	bShowPath = true;
	Server_FindPath(Target, ShowReachable);
}

void UCBGridMovement::Server_FindPath_Implementation(FIntPoint Target, bool ShowReachable)
{
	bShowReachable = ShowReachable;
	if (bShowReachable)
	{
		PathfindingData = WorldManager->Pathfinding->FindPath(Pawn->TileIndex,-999,Travelable,true,MovementPoint);
		PathfindingData.RemoveAt(0);
	}
	else
	{
		PathfindingData = WorldManager->Pathfinding->FindPath(Pawn->TileIndex,Target,Travelable,false,MovementPoint);
	}
	Client_ShowPath();
}

void UCBGridMovement::OnReachTile()
{
	WorldManager->Server_AddOccupant(TargetedTile,Pawn);

	const FIntPoint OriginChunk = WorldManager->GetChunkIndex(OriginTile);
	const FIntPoint TargetChunk = WorldManager->GetChunkIndex(TargetedTile);

	if (OriginChunk.X != TargetChunk.X || OriginChunk.Y != TargetChunk.Y)
	{
		const ACBPlayerController* PlayerController = Cast<ACBPlayerController>(Pawn->GetOwner());
		PlayerController->ChunkManager->Client_RecomputeChunks(TargetChunk);
	}
	
	if (PathfindingData.Num() > 0)
		Server_NextMove();
	else
		Server_FinishMove();
}

void UCBGridMovement::Server_MoveToTile_Implementation()
{
	if (!bIsMoving && !PathfindingData.IsEmpty())
	{
		bIsMoving = true;
		
		SetComponentTickEnabled(true);
		Server_NextMove();
	}
}

void UCBGridMovement::Server_NextMove_Implementation()
{
	WorldManager->Server_RemoveOccupant(Pawn->TileIndex,Pawn);
	OriginTile = TargetedTile;
	TargetedTile = PathfindingData[0];
	TargetLocation = WorldManager->GetTileLocation(TargetedTile) + FVector(
		FMath::RandRange(-40,40),
		FMath::RandRange(-40,40),
		0);
	Pawn->TileIndex = TargetedTile;
	MovementProgress = 0;
	PathfindingData.RemoveAt(0);
	const FRotator LookRotation = UKismetMathLibrary::FindLookAtRotation(Pawn->GetActorLocation(),TargetLocation);
	Pawn->SetActorRotation(LookRotation);
	MovementPoint--;
}

void UCBGridMovement::Server_FinishMove_Implementation()
{
	SetComponentTickEnabled(false);
	bIsMoving = false;
	Pawn->SetActorRotation(FRotator(0,Pawn->GetActorRotation().Yaw,0));
	Pawn->Client_MovementSuccess();
}

TArray<FCBTile> UCBGridMovement::GetNeighbours() const
{
	TArray<FCBTile> Tiles = {};
	const FIntPoint Index = Pawn->TileIndex;
	TArray NeighbourIndexes = {
		FCBPathfindingData(Index + FIntPoint(1,1), Index),
		FCBPathfindingData(Index + FIntPoint(0,2), Index),
		FCBPathfindingData(Index + FIntPoint(-1,1), Index),
		FCBPathfindingData(Index + FIntPoint(-1,-1), Index),
		FCBPathfindingData(Index + FIntPoint(0,-2), Index),
		FCBPathfindingData(Index + FIntPoint(1,-1), Index),
	};
	for (const FCBPathfindingData& NeighbourIndex : NeighbourIndexes)
	{
		FCBFloorData FloorData;
		WorldManager->GetWorldTileData(NeighbourIndex.Index, FloorData);
		FCBTile Tile = FCBTile();
		Tile.Index = NeighbourIndex.Index;
		Tile.Type = FloorData.Type;
		Tiles.Add(Tile);
	}
	return Tiles;
}

