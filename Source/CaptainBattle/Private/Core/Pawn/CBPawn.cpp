#include "Core/Pawn/CBPawn.h"

#include "Core/Pawn/CBGridMovement.h"
#include "Net/UnrealNetwork.h"

ACBPawn::ACBPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	GridMovement = CreateDefaultSubobject<UCBGridMovement>(TEXT("GridMovement"));
	GridMovement->SetIsReplicated(true);
}

void ACBPawn::BeginPlay()
{
	Super::BeginPlay();
	
	SetReplicateMovement(true);

	Server_Init();
}

void ACBPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACBPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACBPawn,TileIndex);
	DOREPLIFETIME(ACBPawn,Name);
}

void ACBPawn::AddResources(const FCBTileResource& TileResource)
{
	const int32* Quantity = Resources.Find(TileResource.ResourceType);
	
	Resources.Add(TileResource.ResourceType, (Quantity ? *Quantity : 0) + TileResource.Quantity);
}

void ACBPawn::RemoveResources(const FCBTileResource& TileResource)
{
	if (const int32* Quantity = Resources.Find(TileResource.ResourceType))
	{
		if (*Quantity - TileResource.Quantity > 0)
		{
			Resources.Add(TileResource.ResourceType, *Quantity - TileResource.Quantity);
		}
		else
		{
			Resources.Remove(TileResource.ResourceType);
		}
	}
}

void ACBPawn::Client_MovementSuccess_Implementation()
{
	OnMovementSuccess.Broadcast();
}

void ACBPawn::Server_Init_Implementation()
{
	GridMovement->Pawn = this;
	GridMovement->MovementPoint = GridMovement->MovementPointMax;
}
